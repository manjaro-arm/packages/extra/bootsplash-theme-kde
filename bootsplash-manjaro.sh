#!/bin/bash

LOGO=KDE.gif
LOGO_WIDTH=$(identify $LOGO | cut -d " " -f 3 | cut -d x -f 1)
LOGO_HEIGHT=$(identify $LOGO | cut -d " " -f 3 | cut -d x -f 2)

convert -alpha remove \
    -background "#000000" \
    $LOGO \
    logo%02d.rgb

bootsplash-packer \
    --bg_red 0x00 \
    --bg_green 0x00 \
    --bg_blue 0x00 \
    --frame_ms 48 \
    --picture \
    --pic_width $LOGO_WIDTH \
    --pic_height $LOGO_HEIGHT \
    --pic_position 0 \
    --pic_anim_type 1 \
    --pic_anim_loop 0 \
    --blob logo00.rgb \
    --blob logo01.rgb \
    --blob logo02.rgb \
    --blob logo03.rgb \
    --blob logo04.rgb \
    --blob logo05.rgb \
    --blob logo06.rgb \
    --blob logo07.rgb \
    bootsplash-manjaro

rm *.rgb
